var box = {
    left: 0,
    top: 0
}
var speed = 20;
var elem = document.querySelector("#box");

function KeyDown() {
    switch (event.key) {
        case "ArrowRight":
            box.left+=speed;
            break;
        case "ArrowLeft":
            box.left-=speed;
            break;
        case "ArrowUp":
            box.top-=speed;
            break;
        case "ArrowDown":
            box.top+=speed;
            break;
    }
    elem.style.left = box.left + "px";
    elem.style.top = box.top + "px";
}

GeneratePoints();

function MovePoints() {
    var points = document.querySelectorAll(".point");
    for (const elem of points) {
        var randLeft = Math.floor(Math.random() * 200) - 100;
        var randTop = Math.floor(Math.random() * 200) - 100;
        var realLeft = getComputedStyle(elem).left;
        var realTop = getComputedStyle(elem).top;
        elem.style.left = parseInt(getComputedStyle(elem).left) + randLeft + 'px';
        elem.style.top = parseInt(getComputedStyle(elem).top) + randTop + 'px';
        //elem.style.transition = Math.sqrt(Math.pow(Math.abs(randLeft - realLeft), 2) + Math.pow(Math.abs(randTop - realTop), 2)) + 's';
    }
}

function Move() {
    elem.style.left = event.clientX - 50 + "px";
    elem.style.top = event.clientY - 50 + "px";
}

function GeneratePoints() {
    for (let i = 0; i < 100; i++) {
        var elem = document.createElement("div");
        elem.className = "point";
        elem.style.left = `${Math.floor(Math.random() * screen.width)}px`;
        elem.style.top = `${Math.floor(Math.random() * screen.height )}px`;
        elem.style.background = GetRandomColor();
        document.body.appendChild(elem);
    
        console.log(elem.style.backgroundColor);
        console.log(getComputedStyle(elem).backgroundColor);
    }
}

function GetRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

setInterval(() => MovePoints(), 500);