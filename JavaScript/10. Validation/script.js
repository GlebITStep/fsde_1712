document.addEventListener("DOMContentLoaded", () => {

    let form = document.forms.myform;
    form.addEventListener("submit", () => {
        console.log(document.forms.myform.checkValidity());
        event.preventDefault();
    });

    let elems = form.elements;
    for (const elem of elems) {     
        elem.addEventListener("focus", () => {
            elem.classList.remove('is-invalid');
        });

        elem.addEventListener("blur", () => {
            if (!elem.checkValidity()) {
                elem.classList.add('is-invalid');
            } else {
                elem.classList.remove('is-invalid');
            }
        });
    }

});