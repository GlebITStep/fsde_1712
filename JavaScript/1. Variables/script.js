// Test('Hello!')

//function declaration
// function Test(name) {
//     console.log(name); 
// };


// Test('Hello2!')



// func('Test');

// //function expression
// var func = function(name) {
//     console.log(name); 
// }

// func('Test2');




// (function Test(name) {
//     console.log(name); 
// })('Test');




// //arrow function
// var func = name => console.log(name);
// func('Test');
// console.log(func);




// var num1 = 42;
// var num2 = new Number(42);
// console.log(num1);
// console.log(num2);


// var str1 = "Hello!";
// var str2 = new String("Hello!");
// console.log(typeof(str1));
// console.log(typeof(str2));


// console.log(-2 + '5');
// console.log('2' - 5);


// var x = parseInt('A');
// console.log(x);


// console.log(3 / 0);
// var y = Infinity / 0;
// console.log(y);




// var num = 123.456;
// console.log(num.toFixed(2));

// console.log(Math.floor(num));
// console.log(Math.ceil(num));
// console.log(Math.round(num));

// console.log(Math.floor(num * 100) / 100);




// var result = (0.1 + 0.2).toFixed(1);
// console.log(result);
// if (result == 0.3) 
//     console.log('Yes!'); 
// else 
//     console.log('No!');

// console.log(9999999999999999);



// let x = 5;
// const y = 10;
// y = 15;
// if(true) {
// 	let x = 10;
// }
// console.log(x);




// var obj1 = new Object();
// var obj2 = {};
// console.log(obj1);
// console.log(obj2);



// var obj = {};
// obj.x = 42;
// obj.y = 'test';
// obj.func = function() {
//     console.log('Test'); 
// }
// console.log(obj);




// var genderEnum = {
//     Male: 0,
//     Female: 1
// }

// var obj = {
//     name: 'Gleb',
//     surname: 'Skripnikov',
//     age: 24,
//     gender: genderEnum.Male,
//     x: obj,

//     fullname: function () {
//         //return this.name + " " + this.surname;
//         this.x = this;
//         return `${this.name} ${this.surname}`;
//     }
// }

// obj.fullname();

// // console.log(obj.fullname());
// // console.log(obj.age);
// // console.log(obj['x']);


// // for (var key in obj) {
// //     console.log(key);
// //     console.log(obj[key]);
// // }

// function Copy(src) {
//     let target = {};
//     for (let prop in src) {
//         if (src.hasOwnProperty(prop)) {
//             target[prop] = src[prop];
//         }
//     }
//     return target;
// }

// function jsonCopy(src) {
//     return JSON.parse(JSON.stringify(src));
// }

// function bestCopyEver(src) {
//     return Object.assign({}, src);
// }

// var obj2 = jsonCopy(obj);
// obj2.name = 'Qalib';
// console.log(obj);
// console.log(obj2);
// console.log(this);






var arr = ['One', 'Two', 'Three', 4];
arr.test = 'TEST';
console.log(arr);




