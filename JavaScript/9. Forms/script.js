document.addEventListener("DOMContentLoaded", () => {
    // // var obj = { name: 'Gleb', age: 24 };
    // // localStorage.setItem("contact", JSON.stringify(obj));

    // // var obj = localStorage.getItem("contact");
    // // console.log(JSON.parse(obj));

    document.forms.myForm.addEventListener("submit", OnColorSelected);
    document.forms.myForm.delete.addEventListener("click", DeleteCookie);

    try {
        let cookies = document.cookie.split('; ');
        let color = cookies.filter(x => x.indexOf('color') == 0)[0].split('=')[1];
        document.body.style.background = color;
    } catch (err) {
        console.log(err);
    }
});

function OnColorSelected() {
    let color = document.forms.myForm.color.value;
    document.body.style.background = color;
    let date = new Date(Date.now());    
    date.setDate(date.getDate() + 3);    
    document.cookie = `color=${color};expires=${date.toUTCString()};path=/`;
    event.preventDefault();
}

function DeleteCookie() {
    document.cookie = `color=;expires=${new Date(0)};path=/`;
}