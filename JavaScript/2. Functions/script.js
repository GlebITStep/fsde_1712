// var arr = [1,2,3,4,5];
// arr.key = "Hello!";
// arr.gleb = 24;
// arr[9] = 10;
// console.log(arr);

// //FOR
// for (let i = 0; i < arr.length; i++) {
//     console.log(arr[i]); 
// }

// //FOR IN
// for (const key in arr) {
//     console.log(arr[key]);
// }

// //FOR OF
// for (const iter of arr) {
//     console.log(iter);
// }

// //FOREACH
// arr.forEach(function(value, index) {
//     console.log(`${index}: ${value}`); 
// });

// //FOREACH (Arrow function)
// arr.forEach((value, index) => console.log(`${index}: ${value}`));



// var text = 'one two three four';
// var arr = text.split(' ');
// console.log(arr);
// console.log(arr.join('+'));



//var arr = ['one', 'two', 'three', 'four', 'five'];
// arr.pop();
// arr.push(11);
// arr.shift();
// arr.unshift(12);
// console.log(arr);
// delete arr[3];
// console.log(arr);

// arr.splice(-3, 2);
// console.log(arr);

// arr.splice(1, 0, 'Gleb', 'Laman');
// console.log(arr);

// console.log(arr);
// arr = arr.slice(3);
// console.log(arr);

// //-1 0 1
// function compare(x, y) {
//     return x - y; 
//     // if (x > y) return 1;
//     // else if (x < y) return -1;
//     // else return 0;
// }

// var arr = [1,5,100000,2,'A',3,4,121];
// arr.sort((x, y) => 
// {
//     if (isNaN(x))
//         return 1;
//     else if (isNaN(y))
//         return -1;
//     else 
//         return x - y;
// });
// console.log(arr);



// var arr = [1,2,3,4,5,6];
// arr = arr.filter(x => x % 2 == 0).filter(x => x > 5);
// console.log(arr);



// var arr2 = ['one', 'two', 'three', 'four', 'five'];
// console.log(arr2);
// arr2 = arr2.map(x => x.toUpperCase() + '!');
// console.log(arr2);



// var arr3 = [1,2,3,4,5,6];
// var result = arr3.reduce((sum, elem, index) => sum -= elem + index);
// console.log(result);


// var arr4 = [6,-8,3,7,0,6];
// var result2 = arr4.reduce((savedIndex, elem, index) => 
// { 
//     console.log(`${savedIndex} ${elem}`);
//     return 0;
// });
// console.log(result2);



// var arr4 = [6,8,3,7,1,6];
// var res = arr4.filter(x => x > 0).length == arr4.length;
// console.log(res);



// //LexicalEnvironment
// //[[Scope]]

// //window { x: undefined, y: undefined, func: function() {...} }

// var x = 0;
// //window { x: 0, y: undefined, func: function() {...} }

// if (true) {
//     var y = 123;
// }
// //window { x: 0, y: 123, func: function() {...} }

// function func() {
//     //funcLE { y: undefined }
//     console.log(x); //[[Scope]].x 
//     console.log(y);
//     var y = 456; //funcLE.y = 456
//     console.log(y);
//     //funcLE { y: 456 }
// }
// //window { x: 0, y: 123, func: function() {...} }

// func();

// console.log(window.x) //0
// console.log(window.y); //123
// console.log(window.num); //undefined


// //window { func1: function() {...} }
// function func1() {
//     //func1LE { x: undefined, func2: function() {...} }
//     var x = 0;

//     function func2() {
//         //func2LE { y: undefined }
//         var y = 0;
//         console.log(x);    
//     }
// }


// function counter() {
//     var i = 0;
//     console.log(i++);
// }
// counter(); //0
// counter(); //1
// counter(); //2






function createCounter() {
    //createCounterLE { i: undefined }
    var i = 0;
    //createCounterLE { i: 0 }
    return function() {
        console.log(i++);        
    }
}

var counter = createCounter();
counter(); //0
counter(); //1
counter(); //2

var counter2 = createCounter();
counter2(); //0
counter2(); //1
counter();  //3













