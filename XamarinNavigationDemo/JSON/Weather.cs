﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using XamarinWeatherApp.Tools;

namespace XamarinWeatherApp.Model
{
    [JsonConverter(typeof(JsonPathConverter))]
    class Weather : ObservableObject
    {
        private int cityId;
        [JsonProperty("id")]
        public int CityId { get => cityId; set => Set(ref cityId, value); }

        private string city;
        [JsonProperty("name")]
        public string City { get => city; set => Set(ref city, value); }

        private string country;
        [JsonProperty("sys.country")]
        public string Country { get => country; set => Set(ref country, value); }

        private int temp;
        [JsonProperty("main.temp")]
        public int Temp { get => temp; set => Set(ref temp, value); }

        private int pressure;
        [JsonProperty("main.pressure")]
        public int Pressure { get => pressure; set => Set(ref pressure, value); }

        private int humidity;
        [JsonProperty("main.humidity")]
        public int Humidity { get => humidity; set => Set(ref humidity, value); }

        private double windSpeed;
        [JsonProperty("wind.speed")]
        public double WindSpeed { get => windSpeed; set => Set(ref windSpeed, value); }

        private int windDeg;
        [JsonProperty("wind.deg")]
        public int WindDeg { get => windDeg; set => Set(ref windDeg, value); }

        private string main;
        [JsonProperty("weather[0].main")]
        public string Main { get => main; set => Set(ref main, value); }

        private string description;
        [JsonProperty("weather[0].description")]
        public string Description { get => description; set => Set(ref description, value); }

        private string icon;
        [JsonProperty("weather[0].icon")]
        public string Icon { get => icon; set => Set(ref icon, value); }

        private int dateSeconds;
        [JsonProperty("dt")]
        public int DateSeconds { get => dateSeconds; set => Set(ref dateSeconds, value); }

        private double lat;
        [JsonProperty("coord.lat")]
        public double Lat { get => lat; set => Set(ref lat, value); }

        private double lon;
        [JsonProperty("coord.lon")]
        public double Lon { get => lon; set => Set(ref lon, value); }
    }
}
