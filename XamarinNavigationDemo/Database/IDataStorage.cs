﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XamarinWeatherApp.Model;

namespace XamarinWeatherApp.Services
{
    interface IDataStorage : IWeatherInfo
    {
        void AddCityAsync(City city);
        void SaveWeatherAsync(Weather weather);
        Task<IEnumerable<City>> GetCitiesAsync();
        Task<City> GetCityByIdAsync(int id);
    }
}
