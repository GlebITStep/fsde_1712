﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XamarinWeatherApp.DependencyServices;
using XamarinWeatherApp.Model;

namespace XamarinWeatherApp.Services
{
    class DataStorage : IDataStorage
    {
        private readonly SQLiteAsyncConnection db;

        public DataStorage()
        {
            var fs = DependencyService.Get<IFileSystem>();
            var dbpath = fs.GetPath("db.db3");
            db = new SQLiteAsyncConnection(dbpath);
            InitializeDatabase();
        }

        private async void InitializeDatabase()
        {
            await db.CreateTableAsync<City>();
            await db.CreateTableAsync<Weather>();
        }

        public async void AddCityAsync(City city)
        {
            await db.InsertAsync(city);
        }

        public async Task<IEnumerable<City>> GetCitiesAsync()
        {
            return await db.Table<City>().ToListAsync();
        }

        public Task<City> GetCityByIdAsync(int id)
        {
            return db.GetAsync<City>(id);
        }

        public async Task<Weather> GetCurrentWeatherAsync(string city)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Weather>> GetForecastAsync(string city)
        {
            throw new NotImplementedException();
        }

        public async void SaveWeatherAsync(Weather weather)
        {
            throw new NotImplementedException();
        }
    }
}
