import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService<T> {
  state: BehaviorSubject<T>;

  constructor() {
    this.state = new BehaviorSubject<T>(null);
  }

  setData(data: T) {
    this.state.next(data);
  }

  getData() : T {
    return this.state.getValue();
  }
}
