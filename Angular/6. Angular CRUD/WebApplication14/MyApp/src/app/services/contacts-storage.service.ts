import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataStorageService } from './data-storage.service';
import { Contact } from '../models/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactsStorageService extends DataStorageService<Contact[]> {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient: HttpClient) {
    super();

    let self = this;
    this.httpClient.get<Array<Contact>>('/api/Contacts')
      .subscribe({
        next(res) { self.setData(res) },
        error(err) { console.log(err) }
      });
  }

  add(contact: Contact) {
    let self = this;
    this.httpClient.post('http://localhost:56539/api/Contacts', contact, this.httpOptions)
      .subscribe({
        next(res) {
          let data = self.state.getValue();
          data.push(res as Contact);
          self.setData(data);
          console.log(res);
        },
        error(err) { console.log(err) }
      });
  }

  delete(id: number) {
    let self = this;
    this.httpClient.delete(`http://localhost:56539/api/Contacts/${id}`, this.httpOptions)
      .subscribe({
        next(res) {
          let data = self.state.getValue();
          self.setData(data.filter(x => x.id != id));
        },
        error(err) { console.log(err) }
      });
  }

  get(id: number) {
    return this.httpClient.get<Contact>(`/api/Contacts/${id}`);
  }

  edit(contact: Contact) {
    let self = this;
    this.httpClient.put('http://localhost:56539/api/Contacts', contact, this.httpOptions)
      .subscribe({
        next(res) {
          let data = self.state.getValue().filter(x => x.id != contact.id);
          data.push(contact);
          self.setData(data);
        },
        error(err) { console.log(err) }
      });
  }
}
