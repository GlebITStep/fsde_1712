import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Contact } from '../../models/contact';
import { ContactsStorageService } from '../../services/contacts-storage.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  id: number;
  contact: Contact;

  constructor(
    private contactsStorage: ContactsStorageService,
    private router: Router,
    private route: ActivatedRoute) {
      
    }

    async ngOnInit() {
      this.id = this.route.snapshot.params.id;
      this.contact = await this.contactsStorage.get(this.id).toPromise();
    }
}
