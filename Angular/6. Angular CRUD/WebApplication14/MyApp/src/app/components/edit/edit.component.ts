import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ContactsStorageService } from '../../services/contacts-storage.service';
import { Contact } from '../../models/contact';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  id: number;
  contact: Contact;
  // name: string;
  // surname: string;
  // phone: string;
  // email: string;

  constructor(
    private contactsStorage: ContactsStorageService,
    private router: Router,
    private route: ActivatedRoute) {
      
    }

  async ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.contact = await this.contactsStorage.get(this.id).toPromise();
  }

  onSubmit() {
    this.contact.id = this.id;
    this.contactsStorage.edit(this.contact);
    this.router.navigate(['/list'])
  }
}
