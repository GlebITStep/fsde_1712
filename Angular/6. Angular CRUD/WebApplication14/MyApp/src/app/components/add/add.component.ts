import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { Contact } from '../../models/contact';
import { ContactsStorageService } from '../../services/contacts-storage.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  contact: Contact;

  constructor(
    private contactsStorage: ContactsStorageService,
    private router: Router) { 
      this.contact = {};
    }

  ngOnInit() {
  }

  onSubmit() {
    this.contactsStorage.add(this.contact);
    //this.router.navigate(['/list'])
  }
}
