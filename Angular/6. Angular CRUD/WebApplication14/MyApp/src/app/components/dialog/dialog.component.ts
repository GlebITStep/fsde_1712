import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

declare let $: any;

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  @Input() title: string;
  @Input() text: string;
  @Output() result: EventEmitter<boolean>; 
  
  constructor() { 
    this.result = new EventEmitter<boolean>();
    this.title = 'Message';
    this.text = 'Are you sure?';
  }

  ngOnInit() {
  }

  show() {
    $("#dialogModal").modal('show');
  }

  close() {
    $("#dialogModal").modal('hide');
  }

  onOk() {
    this.result.emit(true);
    $("#dialogModal").modal('hide');
  }

  onCancel() {
    this.result.emit(false);
    $("#dialogModal").modal('hide');
  }
}
