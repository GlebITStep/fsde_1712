import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ContactsStorageService } from '../../services/contacts-storage.service';
import { Contact } from '../../models/contact';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit { 
  @ViewChild('deleteDialog') deleteDialog: DialogComponent;
  contacts: Observable<Array<Contact>>;
  id: number;
  
  constructor(
    private contactsStorage: ContactsStorageService,
    private httpClient: HttpClient) { }

  ngOnInit() {
    this.contacts = this.contactsStorage.state;
  }

  onDelete(id: number) {
    this.id = id;
    this.deleteDialog.show();
  }

  onDialogResult(result: boolean) {
    if (result)
      this.contactsStorage.delete(this.id);
  }
}
