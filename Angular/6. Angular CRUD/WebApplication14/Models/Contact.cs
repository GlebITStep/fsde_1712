﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication14.Models
{
    public class Contact
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [MaxLength(50, ErrorMessage = "Name lenght can't be more than 50 symbols")]
        public string Name { get; set; }

        [MaxLength(50, ErrorMessage = "Surname lenght can't be more than 50 symbols")]
        public string Surname { get; set; }

        [Required]
        [Phone(ErrorMessage = "Phone is not valid")]
        public string Phone { get; set; }

        [EmailAddress(ErrorMessage = "Email is not valid")]
        public string Email { get; set; }
    }
}
