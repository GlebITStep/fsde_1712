﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication14.Models;

namespace WebApplication14.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContactsController : ControllerBase
    {
        private readonly ContactsDbContext context;

        public ContactsController(ContactsDbContext context)
        {
            this.context = context;
        }

        //[GET] api/Contacts
        [HttpGet]
        public ActionResult<IEnumerable<Contact>> GetContacts()
        {
            return context.Contacts;
        }

        //[GET] api/Contacts/1
        [HttpGet("{id}")]
        public ActionResult<Contact> GetContactById(int id)
        {
            var contact = context.Contacts.FirstOrDefault(x => x.Id == id);
            if (contact != null)
                return contact;
            return NotFound();
        }

        //[POST] api/Contacts
        [HttpPost]
        public IActionResult AddContact([FromBody] Contact contact)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            context.Contacts.Add(contact);
            context.SaveChanges();
            return Ok(contact);
        }

        //[DELETE] api/Contacts/1
        [HttpDelete("{id}")]
        public IActionResult DeleteContact(int id)
        {
            var contact = context.Contacts.FirstOrDefault(x => x.Id == id);
            if (contact != null)
            {
                context.Contacts.Remove(contact);
                context.SaveChanges();
                return Ok(contact);
            }
            return NotFound();
        }

        //[PUT] api/Contacts
        [HttpPut]
        public IActionResult EditContact([FromBody] Contact contact)
        {
            if (contact == null)
                return BadRequest();
            
            if (!context.Contacts.Any(x => x.Id == contact.Id))
                return NotFound();

            context.Update(contact);
            context.SaveChanges();
            return Ok(contact);
        }
    }
}
