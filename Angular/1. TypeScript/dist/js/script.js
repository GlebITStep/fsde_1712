// //number boolean string
// //any
// let age: number;
// age = 5;
// // age = 'Test';
// // age.x = 5;
// const pi: number = 3.14;
// //pi = 3;
// let tuple: [number, string] = [1, 'Gleb'];
// age = tuple[0];
// let arr1: number[] = [1, 2, 3];
// let arr2: Array<[number, string]> = [[1, 'Gleb'], [2, 'Test']];
// enum Color { Red, Green, Blue };
// let color: Color = Color.Blue;
// function Test(): void {
//     console.log('Test'); 
// }
// function Hello(name: string): void {
//     console.log(`Hello, ${name}`); 
// }
// function Hello2(name: string): string {
//     return `Hello, ${name}`;
// }
var gleb = 'Gleb';
$('#gleb').text(gleb.toLocaleUpperCase());
