export class Task {
    static count: number = 1;

    id: number;
    name: string;
    text: string;
    done: boolean;

    constructor(name: string, text: string) {
        this.id = Task.count++;
        this.name = name;
        this.text = text;
        this.done = false;
    }
}