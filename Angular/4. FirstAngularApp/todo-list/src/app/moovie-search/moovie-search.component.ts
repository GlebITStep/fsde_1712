import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-moovie-search',
  templateUrl: './moovie-search.component.html',
  styleUrls: ['./moovie-search.component.sass']
})
export class MoovieSearchComponent implements OnInit {

  search: string;
  apiUrl: string = 'http://www.omdbapi.com/';
  apikey: string = '2c9d65d5'; 
  data: any;

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    
  }

  async onSearchClick() {
    let options = {
      params: {
        apikey: this.apikey,
        s: this.search
      }
    }

    this.data = await this.http.get(this.apiUrl, options).toPromise();
  }
}
