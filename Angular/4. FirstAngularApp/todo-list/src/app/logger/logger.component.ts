import { Component, OnInit } from '@angular/core';
import { LoggerService } from 'src/services/logger.service';

@Component({
  selector: 'app-logger',
  templateUrl: './logger.component.html',
  styleUrls: ['./logger.component.sass']
})
export class LoggerComponent implements OnInit {

  constructor(
    private logger: LoggerService
  ) { }

  ngOnInit() {
  }

  onClearClick() {
    this.logger.clear();
  }

}
