import { Component, OnInit } from '@angular/core';
import * as CanvasJS from 'canvasjs/dist/canvasjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }
}
