import { Component, OnInit } from '@angular/core';
import { Task } from '../../model/task';
import { TaskRepositoryService } from '../../services/task-repository.service';
import { LoggerService } from 'src/services/logger.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {
  tasks: Task[];
  selectedTask: Task;

  constructor(
    private repository: TaskRepositoryService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.tasks = this.repository.getTasks(); 
  }

  onTaskClick(item: Task) {
    this.selectedTask = item;
    this.logger.log(`Item ${item.name} selected`);
  }
}
