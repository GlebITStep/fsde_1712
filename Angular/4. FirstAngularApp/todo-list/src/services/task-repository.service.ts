import { Injectable } from '@angular/core';
import { Task } from 'src/model/task';

@Injectable({
  providedIn: 'root'
})
export class TaskRepositoryService {
  tasks: Task[];

  constructor() {
    this.tasks = [
      new Task('One', 'Lorem ipsum'),
      new Task('Two', 'Dolor sit amet'),
      new Task('Three', 'Lorem ipsum'),
      new Task('Four', 'Dolor sit amet'),
    ]
  }

  getTasks(): Task[] {
    return this.tasks;
  }
}
