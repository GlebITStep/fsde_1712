import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  logs: string[] = [
    "Test1",
    "Test2"
  ];

  constructor() { }

  log(text: string) {
    this.logs.push(text)
  }

  clear() {
    this.logs = [];
  }
}
