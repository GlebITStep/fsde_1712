import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../data-storage.service';
import { ContactsStorageService } from '../contacts-storage.service';
import { Contact } from '../contact';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  text: string = 'Test';
  age: number = 0;

  constructor(
    private dataStorage: DataStorageService<string>,
    private contactsStorage: ContactsStorageService) { }

  ngOnInit() {
  }

  onClick() {
    this.dataStorage.setData(this.text);
    this.contactsStorage.add(new Contact(this.text, this.age));
  }
}
