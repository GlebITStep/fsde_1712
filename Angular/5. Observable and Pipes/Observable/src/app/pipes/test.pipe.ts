import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trim'
})
export class TestPipe implements PipeTransform {

  transform(value: string, length?: number, start: number = 0): string {
    console.log(length); 
    return value.substr(start, length);
  }

}
