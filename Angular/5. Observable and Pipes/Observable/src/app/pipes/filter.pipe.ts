import { Pipe, PipeTransform } from '@angular/core';
import { Contact } from '../contact';

@Pipe({
  name: 'orderByAge',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(value: Contact[]): Contact[] {

    return value.sort((x, y) => x.age - y.age);
  }

}
