import { Injectable } from '@angular/core';
import { DataStorageService } from './data-storage.service';
import { Contact } from './contact';

@Injectable({
  providedIn: 'root'
})
export class ContactsStorageService extends DataStorageService<Contact[]> {

  constructor() {
    super();
    super.setData([]);
  }

  add(contact: Contact) {
    let data = this.state.getValue();
    data.push(contact);
    super.setData(data);
  }
}
