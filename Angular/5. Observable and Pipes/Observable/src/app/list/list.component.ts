import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../data-storage.service';
import { Observable } from 'rxjs';
import { ContactsStorageService } from '../contacts-storage.service';
import { Contact } from '../contact';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  // item: string;
  // item: Observable<string>;
  contacts: Observable<Contact[]>;
  length: number = 5;

  constructor(
    private dataService: DataStorageService<string>,
    private contactsStorage: ContactsStorageService) { }

  ngOnInit() {
    // this.dataService.state.subscribe(x => this.item = x);
    // this.item = this.dataService.state;
    this.contacts = this.contactsStorage.state;
  }

}
