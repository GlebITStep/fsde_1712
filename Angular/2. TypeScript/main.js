var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("person", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Person = /** @class */ (function () {
        function Person(name, age) {
            if (name === void 0) { name = 'Empty'; }
            if (age === void 0) { age = 0; }
            this.name = name;
            this.age = age;
        }
        Person.prototype.info = function () {
            console.log('Person!');
        };
        return Person;
    }());
    exports.Person = Person;
    exports.Text = 'Text';
});
// interface IInformable {
//     info(): void;
// }
define("script", ["require", "exports", "person"], function (require, exports, person_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    console.log(person_1.Text);
    var Student = /** @class */ (function (_super) {
        __extends(Student, _super);
        function Student(name, age, average) {
            if (name === void 0) { name = 'Empty'; }
            if (age === void 0) { age = 0; }
            if (average === void 0) { average = 0; }
            var _this = _super.call(this, name, age) || this;
            _this.average = average;
            return _this;
        }
        Object.defineProperty(Student.prototype, "Average", {
            get: function () { return this.average; },
            set: function (value) { this.average = value; },
            enumerable: true,
            configurable: true
        });
        Student.prototype.info = function () {
            _super.prototype.info.call(this);
            console.log('Student!');
        };
        return Student;
    }(person_1.Person));
    var obj = new Student('Gleb', 24, 12);
    obj.Average = 11;
    console.log(obj.Average);
    obj.info();
});
// function test<T>(param: T): void {
//     console.log(param); 
// }
// test(9);
// test('Gleb');
