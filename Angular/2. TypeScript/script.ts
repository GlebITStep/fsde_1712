// interface IInformable {
//     info(): void;
// }

// interface ITest {
//     name: string;
// }

// class Person implements IInformable, ITest {
// class Person {
//     name: string;
//     age: number;

//     constructor(name: string = 'Empty', age: number = 0) {
//         this.name = name;
//         this.age = age;
//     }

//     info(): void {
//         console.log('Person!'); 
//     }
// }

// let p1: Person = new Person('Gleb', 24); 

// let p2: Person = new Person(); 
// p2.age = 24;
// p2.name = 'Gleb';

// let p3: Person = { name: 'Gleb', age: 24 };

// console.log(p1);
// console.log(p2);
// console.log(p3); 

import { Text, Person } from "./person"

console.log(Text);

class Student extends Person {
    private average: number; 
    get Average() { return this.average }
    set Average(value: number) { this.average = value }
    
    constructor(name: string = 'Empty', age: number = 0, average: number = 0) {
        super(name, age);
        this.average = average;
    }

    info(): void {
        super.info();
        console.log('Student!'); 
    }
}

let obj: Student = new Student('Gleb', 24, 12);
obj.Average = 11;
console.log(obj.Average);
obj.info();


// function test<T>(param: T): void {
//     console.log(param); 
// }

// test(9);
// test('Gleb');