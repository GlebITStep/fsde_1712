export class Person {
    name: string;
    age: number;

    constructor(name: string = 'Empty', age: number = 0) {
        this.name = name;
        this.age = age;
    }

    info(): void {
        console.log('Person!'); 
    }
}

export let Text: string = 'Text';