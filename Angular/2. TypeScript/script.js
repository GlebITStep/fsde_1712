"use strict";
// interface IInformable {
//     info(): void;
// }
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
// interface ITest {
//     name: string;
// }
// class Person implements IInformable, ITest {
// class Person {
//     name: string;
//     age: number;
//     constructor(name: string = 'Empty', age: number = 0) {
//         this.name = name;
//         this.age = age;
//     }
//     info(): void {
//         console.log('Person!'); 
//     }
// }
// let p1: Person = new Person('Gleb', 24); 
// let p2: Person = new Person(); 
// p2.age = 24;
// p2.name = 'Gleb';
// let p3: Person = { name: 'Gleb', age: 24 };
// console.log(p1);
// console.log(p2);
// console.log(p3); 
var person_1 = require("./person");
console.log(person_1.Text);
var Student = /** @class */ (function (_super) {
    __extends(Student, _super);
    function Student(name, age, average) {
        if (name === void 0) { name = 'Empty'; }
        if (age === void 0) { age = 0; }
        if (average === void 0) { average = 0; }
        var _this = _super.call(this, name, age) || this;
        _this.average = average;
        return _this;
    }
    Object.defineProperty(Student.prototype, "Average", {
        get: function () { return this.average; },
        set: function (value) { this.average = value; },
        enumerable: true,
        configurable: true
    });
    Student.prototype.info = function () {
        _super.prototype.info.call(this);
        console.log('Student!');
    };
    return Student;
}(person_1.Person));
var obj = new Student('Gleb', 24, 12);
obj.Average = 11;
console.log(obj.Average);
obj.info();
// function test<T>(param: T): void {
//     console.log(param); 
// }
// test(9);
// test('Gleb');
