"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Person = /** @class */ (function () {
    function Person(name, age) {
        if (name === void 0) { name = 'Empty'; }
        if (age === void 0) { age = 0; }
        this.name = name;
        this.age = age;
    }
    Person.prototype.info = function () {
        console.log('Person!');
    };
    return Person;
}());
exports.Person = Person;
exports.Text = 'Text';
