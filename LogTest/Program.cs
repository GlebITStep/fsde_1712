﻿using Serilog;
using Serilog.Sinks.Slack.Core;
using System;

namespace LogTest
{
    class Program
    {
        static string webHook = "https://hooks.slack.com/services/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";

        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Slack(webHook)
                .CreateLogger();

            Console.WriteLine("First number:");
            int x = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Second number:");
            int y = Int32.Parse(Console.ReadLine());
            try
            {
                Console.WriteLine($"Result: {x / y}");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}
