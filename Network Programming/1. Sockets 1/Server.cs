﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp21
{
    class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint endPoint = new IPEndPoint(
                IPAddress.Parse("127.0.0.1"), 27020);

            Socket socket = new Socket(
                AddressFamily.InterNetwork,
                SocketType.Stream,
                ProtocolType.Tcp);

            socket.Bind(endPoint);

            socket.Listen(200);
            Console.WriteLine("Server started...");

            Socket handler = socket.Accept();

            while (true)
            {
                byte[] bytes = new byte[1000];
                int count = handler.Receive(bytes);
                Console.WriteLine($"{count} bytes received");
                var message = Encoding.Unicode.GetString(bytes, 0, count);
                if (message == "list")
                {

                }
                Console.WriteLine(message);
                Console.WriteLine("End!");
            }


        }
    }
}
