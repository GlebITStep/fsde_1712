﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Client
    {
        static void Main(string[] args)
        {
            SendMessage();
            Console.ReadKey();
        }

        private static async void SendMessage()
        {
            var http = new HttpClient();
            http.BaseAddress = new Uri("http://localhost:8080/");



            var json = @"{
                'ProductName': 'Alex',
                'SupplierId': 1,
                'UnitPrice': 999,
                'Package': 'Box',
                'IsDiscontinued': true
            }";
            var content = new StringContent(json);
            await http.PostAsync("products", content);



            //await http.DeleteAsync("products/82");



            //var data = await http.GetAsync("products/82");
            //var str = await data.Content.ReadAsStringAsync();
            //Console.WriteLine(str);

            //var data = await http.GetAsync("products");
            //var str = await data.Content.ReadAsStringAsync();
            //Console.WriteLine(str);
        }
    }
}
