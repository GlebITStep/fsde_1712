﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Server
    {
        static void Main(string[] args)
        {
            StartServer();
            Console.ReadKey();
        }

        private static async void StartServer()
        {
            var server = new HttpListener();
            server.Prefixes.Add("http://127.0.0.1:8080/");
            server.Start();

            while (true)
            {
                var context = await server.GetContextAsync();
                var request = context.Request;
                var response = context.Response;

                Console.WriteLine(request.RawUrl);
                var method = request.HttpMethod;

                if (request.RawUrl == "/favicon.ico" || request.RawUrl == "/robots.txt")
                {
                    using (var stream = new StreamWriter(response.OutputStream))
                    {
                        stream.WriteLine("");
                    }
                    continue;
                }

                //localhost/products/12
                var segments = request.RawUrl.Split('/');
                string table = null;
                string id = null;
                try
                {
                    table = segments[1];
                    id = segments[2];
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Console.WriteLine(id);

                if (method == "GET")
                {
                    if (table == "products" && id != null)
                    {
                        var productId = Int32.Parse(id);
                        using (var stream = new StreamWriter(response.OutputStream))
                        {
                            using (var db = new ShopModel())
                            {
                                var product = db.Products
                                    .Where(x => x.Id == productId)
                                    .FirstOrDefault();
                                var json = JsonConvert.SerializeObject(product);
                                stream.WriteLine(json);
                            }
                        }
                    }
                    else if (table == "products")
                    {
                        using (var stream = new StreamWriter(response.OutputStream))
                        {
                            using (var db = new ShopModel())
                            {
                                var list = db.Products.ToList();
                                var json = JsonConvert.SerializeObject(list);
                                stream.WriteLine(json);
                            }
                        }
                    }
                }
                else if (method == "DELETE")
                {
                    if (table == "products" && id != null)
                    {
                        var productId = Int32.Parse(id);
                        using (var stream = new StreamWriter(response.OutputStream))
                        {
                            using (var db = new ShopModel())
                            {
                                db.Products.Remove(
                                    db.Products
                                    .Where(x => x.Id == productId)
                                    .FirstOrDefault()
                                    );
                                db.SaveChanges();
                            }
                        }
                    }
                }
                else if (method == "POST")
                {
                    if (table == "products")
                    {
                        var json = "";
                        using (var stream = new StreamReader(request.InputStream))
                        {
                            json = stream.ReadToEnd();
                        }

                        var product = JsonConvert.DeserializeObject<Product>(json);
                        Console.WriteLine(product.ProductName);
                        
                        using (var stream = new StreamWriter(response.OutputStream))
                        {
                            using (var db = new ShopModel())
                            {
                                db.Products.Add(product);
                                db.SaveChanges();
                                stream.WriteLine("Done!");
                            }
                        }
                    }
                }
            }
        }
    }
}
