using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    //27018
    class Program
    {
        static void Main(string[] args)
        {
            StartServer();
            Console.ReadKey();
        }

        private static async void StartServer()
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://*:80/");
            listener.Start();

            while (true)
            {
                try
                {
                    var context = await listener.GetContextAsync();
                    var request = context.Request;
                    var response = context.Response;

                    Console.WriteLine(request.Url);
                    Console.WriteLine(request.RemoteEndPoint);

                    if (request.RawUrl == "/favicon.ico")
                    {
                        var bytes = File.ReadAllBytes("cat.ico");
                        using (var stream = new BinaryWriter(response.OutputStream))
                        {
                            stream.Write(bytes);
                        }
                    }
                    else
                    {
                        using (var stream = new StreamWriter(response.OutputStream))
                        {
                            stream.WriteLine(@"<h1>Hello!</h1>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
