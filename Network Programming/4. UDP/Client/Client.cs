﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Client
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Client");
            UdpClient receiver = new UdpClient(8080);
            IPEndPoint endPoint = null;
            while (true)
            {
                var bytes = receiver.Receive(ref endPoint);
                var message = Encoding.Unicode.GetString(bytes);
                Console.WriteLine($"{endPoint} {message}");
            }
        }
    }
}
