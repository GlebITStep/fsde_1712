﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Server
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Server");
            UdpClient sender = new UdpClient();
            while (true)
            {
                var message = Console.ReadLine();
                var bytes = Encoding.Unicode.GetBytes(message);
                sender.Send(bytes, bytes.Length, "127.0.0.1", 8080);
            }
        }
    }
}
