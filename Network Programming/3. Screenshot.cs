//Add System.Drawing reference

private Task<byte[]> CaptureScreenshot()
{
	return Task.Run(() =>
	{
		Thread.Sleep(500);
		Rectangle bounds = new Rectangle(0, 0, 1920, 1080);
		using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
		{
			using (Graphics g = Graphics.FromImage(bitmap))
			{
				g.CopyFromScreen(System.Drawing.Point.Empty, System.Drawing.Point.Empty, bounds.Size);
			}

			using (var stream = new MemoryStream())
			{
				bitmap.Save(stream, ImageFormat.Png);
				return stream.ToArray();
			}
		}
	});
}