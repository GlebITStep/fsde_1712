﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfApp7.Model;
using WpfApp7.Tools;

namespace WpfApp7.ViewModel
{
    class ContactListViewModel : ObservableObject
    {
        private string test = "Test";
        public string Test
        {
            get => test;
            set { test = value; OnPropertyChanged(); }
        }

        private string inputName;
        public string InputName
        {
            get => inputName;
            set { inputName = value; OnPropertyChanged(); }
        }

        private string inputPhone;
        public string InputPhone
        {
            get => inputPhone;
            set { inputPhone = value; OnPropertyChanged(); }
        }

        public ObservableCollection<Contact> Contacts { get; set; } = new ObservableCollection<Contact>();

        public ContactListViewModel()
        {
            Contacts.Add(new Contact { Name = "Gleb", Phone = "123456789" });
        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ?? (addCommand = new RelayCommand(
                    param =>
                    {
                        Contacts.Add(new Contact { Name = InputName, Phone = InputPhone });
                    }
                ));
            }
        }

        private RelayCommand viewCommand;
        public RelayCommand ViewCommand
        {
            get
            {
                return viewCommand ?? (viewCommand = new RelayCommand(
                    x =>
                    {
                        var param = x as Contact;
                        MessageBox.Show($"{param.Name} {param.Phone}");
                    }
                ));
            }
        }
    }
}
