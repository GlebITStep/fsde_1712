﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MVVMApp2.Messages;
using MVVMApp2.Model;
using MVVMApp2.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMApp2.ViewModel
{
    class ToDoListViewModel : ViewModelBase
    {
        private ObservableCollection<ToDo> list = new ObservableCollection<ToDo>();
        public ObservableCollection<ToDo> List
        {
            get { return list; }
            set { Set(ref list, value); }
        }

        private readonly NavigationService navigation;

        public ToDoListViewModel(NavigationService navigation)
        {
            this.navigation = navigation;

            Messenger.Default.Register<AddMessage>(this,
                param =>
                {
                    List.Add(new ToDo { Title = param.Title, Done = false });
                });
        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get => addCommand ?? (addCommand = new RelayCommand(
                () => navigation.NavigateTo(VM.Add) 
            ));
        }
    }
}
