﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MVVMApp2.Messages;
using MVVMApp2.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
namespace MVVMApp2.ViewModel
{
    class AddViewModel : ViewModelBase
    {
        private string input;
        public string Input
        {
            get { return input; }
            set { Set(ref input, value); }
        }

        private readonly NavigationService navigation;

        public AddViewModel(NavigationService navigation)
        {
            this.navigation = navigation;
        }
    
        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get => addCommand ?? (addCommand = new RelayCommand(
                () =>
                {
                    Messenger.Default.Send(new AddMessage { Title = Input });
                    navigation.NavigateTo(VM.ToDoList);
                }
            ));
        }
    }
}
