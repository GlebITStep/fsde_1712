﻿using MVVMApp2.Navigation;
using MVVMApp2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMApp2
{
    class ViewModelLocator
    {
        public AppViewModel appViewModel;
        public ToDoListViewModel toDoListViewModel;
        public AddViewModel addViewModel;

        private NavigationService navigationService;

        public ViewModelLocator()
        {
            navigationService = new NavigationService();

            appViewModel = new AppViewModel();
            toDoListViewModel = new ToDoListViewModel(navigationService);
            addViewModel = new AddViewModel(navigationService);

            navigationService.AddPage(toDoListViewModel, VM.ToDoList);
            navigationService.AddPage(addViewModel, VM.Add);

            navigationService.NavigateTo(VM.ToDoList);
        }
    }
}
