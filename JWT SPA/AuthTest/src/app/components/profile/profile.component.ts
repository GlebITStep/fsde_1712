import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/services/profile.service';
import { Profile } from 'src/app/models/profile';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileData: Profile;

  constructor(
    private profile: ProfileService
  ) { }

  async ngOnInit() {
    try {
      this.profileData = await this.profile.getCurrentProfile();    
    } catch (error) {
      console.log(error);
    }
  }

}
