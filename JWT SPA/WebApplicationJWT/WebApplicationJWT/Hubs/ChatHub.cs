﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJWT.Hubs
{
    public class ChatHub : Hub
    {
        static public List<string> MyClients { get; set; } = new List<string>();

        public async Task SendMessage(string message)
        {
            await Clients.All.SendAsync("Send", MyClients.Count);
        }

        public async override Task OnConnectedAsync()
        {
            MyClients.Add(Clients.Caller.ToString());
            //base.OnConnectedAsync();
        }
    }
}
