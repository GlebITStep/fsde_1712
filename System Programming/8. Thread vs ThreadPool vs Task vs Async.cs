using System;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp23
{
    class Program
    {
        static void Main(string[] args)
        {
            Start();
            Console.ReadKey();
        }

        private static async void Start()
        {
            //Async
            Console.WriteLine("Start");
            var result = await GetResultAsync();
            Console.WriteLine($"Result: {result}");
            Console.WriteLine("End");

            ////Task
            //Console.WriteLine("Start");
            //GetResultTask();

            ////ThreadPool
            //Console.WriteLine("Start");
            //GetResultThreadPool();

            ////Thread
            //Console.WriteLine("Start");
            //GetResultThread();

            ////Sync
            //Console.WriteLine("Start");
            //var result = GetResultSync();
            //Console.WriteLine($"Result: {result}");
            //Console.WriteLine("End");
        }

        private static int GetResultSync()
        {
            Thread.Sleep(1000);
            var result = 42;
            return result;
        }

        private static void GetResultThread()
        {
            var thread = new Thread(new ThreadStart(() =>
            {
                Thread.Sleep(1000);
                var result = 42;
                Callback(result);
            }));
            thread.Start();
        }

        private static void GetResultThreadPool()
        {
            ThreadPool.QueueUserWorkItem(x =>
            {
                Thread.Sleep(1000);
                var result = 42;
                Callback(result);
            });
        }

        private static void GetResultTask()
        {
            Task.Run(() =>
            {
                Thread.Sleep(1000);
                var result = 42;
                Callback(result);
            });
        }

        private static Task<int> GetResultAsync()
        {
            return Task.Run(() =>
            {
                Thread.Sleep(1000);
                var result = 42;
                return result;
            });
        }

        private static void Callback(int result)
        {
            Console.WriteLine($"Result: {result}");
            Console.WriteLine("End");
        }
    }
}
