﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPlugin
{
    public interface IPlugin
    {
        string Name { get; set; }
        void Do();
    }
}
