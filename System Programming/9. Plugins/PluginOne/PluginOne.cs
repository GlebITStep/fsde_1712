﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyIPlugin;

namespace MyPluginOne
{
    public class PluginOne : IPlugin
    {
        public string Name { get; set; } = "One";

        public void Do()
        {
            Console.WriteLine("Plugin one action!");
        }
    }

    public class Test
    {
        public int TestProp { get; set; }
    }
}
