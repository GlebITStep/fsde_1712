﻿using MyIPlugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppPlugins
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo di = new DirectoryInfo("Plugins");
            var dll = di.GetFiles();
            var assemblies = new List<Assembly>();
            foreach (var item in dll)
            {
                assemblies.Add(Assembly.LoadFile(item.FullName));
            }

            foreach (var item in assemblies[1].GetTypes())
            {
                if (item.GetInterface("IPlugin") != null)
                {
                    Console.WriteLine(item);
                    IPlugin obj = Activator.CreateInstance(item) as IPlugin;
                    Console.WriteLine(obj);
                    obj.Do();
                }
            }
            
        }
    }
}
