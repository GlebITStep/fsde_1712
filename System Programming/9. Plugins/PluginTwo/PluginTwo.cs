﻿using MyIPlugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPluginTwo
{
    public class PluginTwo : IPlugin
    {
        public string Name { get; set; } = "Two";

        public void Do()
        {
            Console.WriteLine("Plugin two action!");
        }
    }
}
