﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineShop.Models;
using OnlineShop.Models.ViewModels;
using OnlineShop.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShop.Controllers
{
    public class HomeController : Controller
    {
        private IProductsRepository ProductsRepository { get; }
        private ICartService CartService { get; }

        public HomeController(
            IProductsRepository productsRepository,
            ICartService cartService)
        {
            ProductsRepository = productsRepository;
            CartService = cartService;
        }

        public ActionResult Index()
        {
            ViewBag.Name = "Gleb";
            ViewData.Add("Surname", "Skripnikov");
            return View(ProductsRepository.GetProducts());
        }

        public ActionResult Products()
        {
            return View();
        }

        public ActionResult AddToCart(int id)
        {
            CartService.Add(ProductsRepository.GetById(id));
            return RedirectToAction("Index");
        }

        public ActionResult AddProduct(IFormFile image, Product product)
        {
            var fileName = $"{(DateTime.Now - new DateTime(2018,1,1)).TotalMilliseconds}{image.FileName}";
            var filePath = $"{Directory.GetCurrentDirectory()}\\wwwroot\\{fileName}";
            using (var fs = new FileStream(filePath, FileMode.Create))
            {
                image.CopyTo(fs);
                product.ImagePath = fileName;
            }
            ProductsRepository.Add(product);
            TempData["Message"] = "Product added!";
            return RedirectToAction("Index");
        }

        public ActionResult RemoveProduct(int id)
        {
            ProductsRepository.Remove(id);
            TempData["Message"] = "Product deleted!";
            return RedirectToAction("Index");
        }
    }
}
