﻿using OnlineShop.Models;
using OnlineShop.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShop.Services
{
    public interface ICartService
    {
        IEnumerable<CartItemViewModel> Cart();
        void Add(Product product);
    }
}
