﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineShop.Models;
using OnlineShop.Models.ViewModels;

namespace OnlineShop.Services
{
    public class FakeCartService : ICartService
    {
        Dictionary<int, CartItemViewModel> items = new Dictionary<int, CartItemViewModel>();

        public void Add(Product product)
        {
            if (!items.ContainsKey(product.Id))
            {
                items.Add(product.Id, new CartItemViewModel
                {
                    Item = product,
                    Count = 1
                });
            }
            else
            {
                items[product.Id].Count++;
            }
        }

        public IEnumerable<CartItemViewModel> Cart()
        {
            return items.Values;
        }
    }
}
