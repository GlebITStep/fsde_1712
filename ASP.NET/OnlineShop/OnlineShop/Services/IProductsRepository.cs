﻿using OnlineShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShop.Services
{
    public interface IProductsRepository
    {
        Product GetById(int id);
        void Add(Product product);
        void Remove(int id);
        IEnumerable<Product> GetProducts();
    }
}
