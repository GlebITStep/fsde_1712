﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineShop.Models;

namespace OnlineShop.Services
{
    public class FakeProductsRepository : IProductsRepository
    {
        List<Product> products = new List<Product>();
        int counter = 0;

        public FakeProductsRepository()
        {
            Add(new Product
            {
                Name = "Formica ex animale 3/15",
                Description = "blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id",
                Price = 41.20M
            });
            Add(new Product
            {
                Name = "benzalkonium chloride and lidocaine",
                Description = "pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue",
                Price = 77.82M
            });
            Add(new Product
            {
                Name = "Aluminum Sesquichlorohydrate",
                Description = "congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam",
                Price = 15.99M
            });
            Add(new Product
            {
                Name = "Hackberry",
                Description = "quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse",
                Price = 82.80M
            });
            Add(new Product
            {
                Name = "Fentanyl",
                Description = "curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi",
                Price = 1.29M
            });
            Add(new Product
            {
                Name = "nedocromil sodium",
                Description = "gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer",
                Price = 88.31M
            });
        }

        public Product GetById(int id)
        {
            return products.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Product> GetProducts()
        {
            return products;
        }

        public void Add(Product product)
        {
            product.Id = ++counter;
            products.Add(product);
        }

        public void Remove(int id)
        {
            products.Remove(products.FirstOrDefault(x => x.Id == id));
        }

    }
}
