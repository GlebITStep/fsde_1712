﻿using Microsoft.AspNetCore.Mvc;
using OnlineShop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShop.Components
{
    public class CartModal : ViewComponent
    {
        private ICartService CartService { get; }

        public CartModal(ICartService cartService)
        {
            CartService = cartService;
        }

        public IViewComponentResult Invoke()
        {
            return View(CartService.Cart());
        }
    }
}
