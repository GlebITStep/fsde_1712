﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShop.Models.ViewModels
{
    public class CartItemViewModel
    {
        public Product Item { get; set; }
        public int Count { get; set; }
        public decimal TotalPrice { get => Count * Item.Price; }
    }
}
