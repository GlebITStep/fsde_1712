﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BlogWebsite.Models;
using Microsoft.AspNetCore.Http;
using BlogWebsite.Services;
using BlogWebsite.Extensions;

namespace BlogWebsite.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PostsController : Controller
    {
        private readonly NewsPortalContext _context;
        private readonly SaveFileService saveFileService;

        public PostsController(NewsPortalContext context, SaveFileService saveFileService)
        {
            _context = context;
            this.saveFileService = saveFileService;
        }

        public ActionResult UploadImage(IFormFile file)
        {
            var path = saveFileService.SaveFile(file);
            return Json(new { location = path });
        }

        // GET: Admin/Posts
        public async Task<IActionResult> Index()
        {
            var newsPortalContext = _context.Posts.Include(p => p.Category);
            return View(await newsPortalContext.ToListAsync());
        }

        // GET: Admin/Posts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTags)
                .ThenInclude(pt => pt.Tag)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (post == null)
            {
                return NotFound();
            }
return View(post);
            
        }

        // GET: Admin/Posts/Create
        public IActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(_context.Categories, "Id", "Name");
            ViewBag.Tags = new SelectList(_context.Tags, "Id", "Name");
            return View();
        }

        // POST: Admin/Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int[] Tags, [Bind("Id,Header,Description,Content,Date,CategoryId")] Post post)
        {
            if (ModelState.IsValid)
            {
                _context.Add(post);
                await _context.SaveChangesAsync();
                await _context.AddRangeAsync(Tags.Select(
                    x => new PostTag { PostId = post.Id, TagId = x }));
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Name", post.CategoryId);
            return View(post);
        }

        // GET: Admin/Posts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var post = await _context.Posts.FindAsync(id);
            var post = await _context.Posts
                .Include(x => x.PostTags)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (post == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Id", post.CategoryId);
            ViewBag.Tags = new MultiSelectList(_context.Tags, "Id", "Name", post.PostTags.Select(x => x.TagId));
            return View(post);
        }

        // POST: Admin/Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int[] Tags, int id, [Bind("Id,Header,Description,Content,Date,CategoryId")] Post post)
        {
            if (id != post.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(post);
                    await _context.SaveChangesAsync();

                    _context.Set<PostTag>().Load();
                    //var oldTags = _context.Posts
                    //    .Include(x => x.PostTags)
                    //    .FirstOrDefault(x => x.Id == id)
                    //    .PostTags;

                    _context.TryUpdateManyToMany(
                        post.PostTags,
                        Tags.Select(x => new PostTag { PostId = post.Id, TagId = x }),
                        x => x.TagId);

                    //_context.RemoveRange(
                    //    _context.Set<PostTag>()
                    //    .Where(x => x.PostId == post.Id)
                    //    .Where(x => !Tags.Contains(x.TagId)));
                    //// Tags    -> PostTags     
                    //// 3, 6, 7 -> 1 - 3, 1 - 6, 1 - 7

                    //var newTags = Tags
                    //    .Where(x => _context.Set<PostTag>().Find(post.Id, x) == null);
                    //await _context
                    //    .AddRangeAsync(newTags
                    //        .Select(x => new PostTag { PostId = post.Id, TagId = x })); ;

                    ////1 - 5, 1 - 6, 1 - 7

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostExists(post.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Id", post.CategoryId);
            return View(post);
        }

        // GET: Admin/Posts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Posts
                .Include(p => p.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Admin/Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var post = await _context.Posts.FindAsync(id);
            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PostExists(int id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }
    }
}
