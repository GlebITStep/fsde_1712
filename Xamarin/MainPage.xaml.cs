using Plugin.Connectivity;
using SharedProject1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DbApp
{
	public partial class MainPage : ContentPage
	{
        public MainPage()
        {
            InitializeComponent();

            CrossConnectivity.Current.ConnectivityTypeChanged += Current_ConnectivityTypeChanged; 
        }

        private void Current_ConnectivityTypeChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityTypeChangedEventArgs e)
        {
            Layout.Children.Clear();
            foreach (var item in CrossConnectivity.Current.ConnectionTypes)
            {
                Layout.Children.Add(new Label { Text = item.ToString() });
            }
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var conn = CrossConnectivity.Current.IsConnected;
            DisplayAlert("Status", conn.ToString(), "OK");
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            IFileSystem fs = DependencyService.Get<IFileSystem>();
            DisplayAlert("Path", fs.GetPath("file.dat"), "OK");
        }
    }
}
