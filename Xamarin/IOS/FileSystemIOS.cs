using System;
using DbApp.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileSystemIOS))]
namespace DbApp.iOS
{
    class FileSystemIOS : IFileSystem
    {
        public string GetPath(string filename)
        {
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = System.IO.Path.Combine(docFolder, "..", "Library");

            if (!System.IO.Directory.Exists(libFolder))
            {
                System.IO.Directory.CreateDirectory(libFolder);
            }

            return System.IO.Path.Combine(libFolder, filename);
        }
    }
}