using DbApp.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileSystemAndroid))]
namespace DbApp.Droid
{
    class FileSystemAndroid : IFileSystem
    {
        public string GetPath(string filename)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return System.IO.Path.Combine(path, filename);
        }
    }
}