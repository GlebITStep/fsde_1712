using DbApp.UWP;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileSystemWindows))]
namespace DbApp.UWP
{
    class FileSystemWindows : IFileSystem
    {
        public string GetPath(string filename)
        {
            string path = Windows.Storage.ApplicationData.Current.LocalFolder.Path;
            return System.IO.Path.Combine(path, filename);
        }
    }
}
