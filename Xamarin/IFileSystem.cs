namespace DbApp
{
    public interface IFileSystem
    {
        string GetPath(string filename);
    }
}
